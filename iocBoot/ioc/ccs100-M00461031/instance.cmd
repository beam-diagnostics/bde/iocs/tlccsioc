###############################################################################

#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("LOCATION", "LAB")
epicsEnvSet("DEVICE_NAME", "CCS1")
epicsEnvSet("USBVID", "0x1313")
epicsEnvSet("USBPID", "0x8081")
epicsEnvSet("SERNO", "M00461031")
epicsEnvSet("XSIZE", "4000")
epicsEnvSet("YSIZE", "1")
epicsEnvSet("NELEMENTS", "4000")
###############################################################################
